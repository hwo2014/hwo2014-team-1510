
class Bot:

    def __init__(self):
        self.cars = {}
        pass

    def tick(self, message):
        if message["msgType"] == "yourCar":
            self.id = message["data"]
        if message["msgType"] == "gameInit":
            map(self.set_car, [ (car.id, car) for car in init_cars(message["data"]["race"]["cars"]) ])
            self.track = TrackGeometry(message["data"]["race"]["track"])
        if message["msgType"] == "carPositions":
            self.update_car_positions(message["data"])
        time.sleep(0.8*1.0/60.0)
        return {"msgType": "throttle", "data": 1.0}

    def set_car(self, item):
        car_id = item[0]
        key = (car_id["name"], car_id["color"])
        self.cars[key] = item[1]

    def get_car(self, car_id):
        key = (car_id["name"], car_id["color"])
        return self.cars[key] if key in self.cars else None

    def update_car_positions(self, cps):
        t = time.time()
        for cp in cps:
            car = self.get_car(cp["id"])
            car.data["piecePosition"] = cp["piecePosition"]
            car.data["angle"] = cp["angle"]

import sys
import pygame
import pygame.freetype
from math import cos, sin, radians

class TrackGeometry:

    def __init__(self, track):
        self.laneN = 0
        self.pieceN = 0
        self.piece_geom = {}
        self.traverse(track)

    def traverse(self, track):
        self.laneN = len(track["lanes"])
        self.pieceN = len(track["pieces"])
        self.piece_geom = [ [ None for j in range(self.laneN) ] for i in range(self.pieceN) ]
        for lane in track["lanes"]:
            laneIndex = int(lane["index"])
            a = radians(float(track["startingPoint"]["angle"]))
            d = float(lane["distanceFromCenter"])
            da = radians(-90 if d > 0.0 else 90)
            x = float(track["startingPoint"]["position"]["x"]) + d * cos(a+da)
            y = float(track["startingPoint"]["position"]["y"]) + d * sin(a+da)
            for (pieceIndex, piece) in enumerate(track["pieces"]):
                switch = "switch" in piece and piece["switch"]
                if "length" in piece: # straight
                    length = float(piece["length"])
                    x2 = x + length*cos(a)
                    y2 = y + length*sin(a)
                    self.piece_geom[pieceIndex][laneIndex] = ("straight", ((x, y), (x2, y2)), (switch,length))
                    (x, y) = (x2, y2)
                elif "radius" in piece: # bend
                    angle = -radians(float(piece["angle"]))
                    radius = float(piece["radius"]) + d * (1.0 if (d > 0.0 and angle > 0.0) or (d < 0.0 and angle < 0.0) else -1.0)
                    length = radius*abs(angle)
                    # approx with a line
                    a += angle
                    approx_line_length = 2 * radius * sin(abs(angle)/2.0)
                    x2 = x + approx_line_length*cos(a)
                    y2 = y + approx_line_length*sin(a)
                    self.piece_geom[pieceIndex][laneIndex] = ("bend", ((x, y), (x2, y2)), (switch,length,radius))
                    (x, y) = (x2, y2)

    def proceed(self, pp, displacement):
        length = track.piece_geom[pp["pieceIndex"]][pp["lane"]["startLaneIndex"]][2][1]
        pp["inPieceDistance"] += displacement
        if pp["inPieceDistance"] >= length:
            pp["inPieceDistance"] -= length
            pp["pieceIndex"] += 1
            if pp["pieceIndex"] >= self.pieceN:
                pp["pieceIndex"] = 0
                pp["lap"] += 1

class Display:

    def __init__(self, track):
        pygame.init() 
        pygame.font.init()
        self.view_width = 1000
        self.view_height = 1000
        self.view_center = (self.view_width/2, self.view_height/2)
        self.view_scale = (1.0, -1.0)
        self.window = pygame.display.set_mode((self.view_width, self.view_height+32)) 
        self.colors_by_name = { "white": (255,255,255), "cyan": (0,255,255), "magenta": (255,0,255), "yellow": (255,255,0), "red": (255,0,0), "green": (0,255,0), "blue": (0,0,255) }
        self.colors = self.colors_by_name.values()
        self.background_color = (0,0,0)
        self.track = track

    def calibrate(self, minx, maxx, miny, maxy):
        self.track_width = abs(maxx - minx)
        self.track_height = abs(maxy - miny)
        scale = 1.0
        if self.view_width < self.track_width:
            scale *= self.view_width / self.track_width
        if self.view_height < self.track_height * scale:
            scale *= self.view_height / self.track_height
        self.view_scale = (scale, -scale)
        self.track_center = (scale*(minx+maxx)/2, scale*(miny+maxy)/2)

    def transform(self, p):
        x = int(round(self.view_center[0]-self.track_center[0]+self.view_scale[0]*p[0]))
        y = int(round(self.view_center[1]-self.track_center[1]+self.view_scale[1]*p[1]))
        return (x, y)

    def line(self, p1, p2, color):
        pygame.draw.line(self.window, color, self.transform(p1), self.transform(p2))

    def circle(self, p, r, color, width):
        pygame.draw.circle(self.window, color, self.transform(p), r, width)

    def clear(self):
        self.window.fill(self.background_color)

    def show(self):
        pygame.display.flip()

    def text(self, i, text, color="blue"):
        rowheight = 15
        font = pygame.font.SysFont("monospace", 15)
        c = self.colors_by_name[color]
        self.window.blit(font.render(text, 1, c), (100, 100+i*rowheight))

    def draw_track(self):
        seq = [t for t in self.gen_sequence()]
        points = [p for (prim, points, params) in seq for p in points]
        xs = [x for (x, y) in points]
        ys = [y for (x, y) in points]
        self.calibrate(min(xs), max(xs), min(ys), max(ys))
        self.draw_seq(seq)

    def draw_seq(self, seq):
        for (prim, points, params) in seq:
            if prim == "line":
                self.line(points[0], points[1], params[0])
            elif prim == "arc":
                self.line(points[0], points[1], params[1])
            elif prim == "circle":
                self.circle(points[0], params[0], params[1], params[2])

    def gen_sequence(self):
        for laneIndex in range(self.track.laneN):
            color = self.colors_by_name["white"]
            (x0, y0) = self.track.piece_geom[0][laneIndex][1][0]
            yield "circle", ((x0, y0),), (5,color,0)
            for pieceIndex in range(self.track.pieceN):
                pieceGeom = self.track.piece_geom[pieceIndex][laneIndex]
                if pieceGeom[0] == "straight":
                    yield "line", pieceGeom[1], (color,)
                elif pieceGeom[0] == "bend":
                    yield "arc", pieceGeom[1], (pieceGeom[2][2],color)
                if pieceGeom[2][0]:
                    ((x1, y1), (x2, y2)) = pieceGeom[1]
                    c = ((x1+x2)/2, (y1+y2)/2)
                    yield "circle", (c,), (10,color,0)

    def calc_car(self, cp):
        pieceIndex = int(cp["piecePosition"]["pieceIndex"])
        laneIndex = int(cp["piecePosition"]["lane"]["startLaneIndex"])
        inPieceDistance = float(cp["piecePosition"]["inPieceDistance"])
        length = self.track.piece_geom[pieceIndex][laneIndex][2][1]
        alfa = inPieceDistance / length
        ((x1, y1), (x2, y2)) = self.track.piece_geom[pieceIndex][laneIndex][1]
        x = x1 * (1-alfa) + x2 * alfa
        y = y1 * (1-alfa) + y2 * alfa
        c = self.colors_by_name[cp["id"]["color"]]
        return (x, y, c)

    def draw_car(self, cp):
        (x, y, c) = self.calc_car(cp)
        seq = [ ("circle", ((x, y),), (5, c, 0)) ]
        self.draw_seq(seq)

    def draw_car_belief(self, car0_id, cp):
        c0 = self.colors_by_name[car0_id["color"]]
        (x, y, c) = self.calc_car(cp)
        seq = [
                ("circle", ((x, y),), (7, c0, 2)),
                ("circle", ((x, y),), (9, c, 2))
            ]
        self.draw_seq(seq)

    def loop(self):
        pygame.time.Clock().tick(100)
        while True: 
            for event in pygame.event.get(): 
                if event.type == pygame.QUIT: 
                    pygame.freetype.quit()
                    pygame.quit
                    return
                else: 
                    pass


yourCar = {"msgType": "yourCar", "data": { "name": "Schumacher", "color": "red" } }

gameInit = { "msgType": "gameInit", "data": {
            "race": {
                "track": {
                    "id": "indianapolis",
                    "name": "Indianapolis",
                    "pieces": [
                        { "length": 100.0 },
                        { "length": 100.0, "switch": True },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5, "switch": True },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "length": 100.0 },
                        { "length": 100.0 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 },
                        { "radius": 100, "angle": 22.5 }
                        ],
                    "lanes": [
                            { "distanceFromCenter": -5, "index": 0 },
                            { "distanceFromCenter": 0, "index": 1 },
                            { "distanceFromCenter": 20, "index": 2 }
                            ],
                    "startingPoint": {
                            "position": { "x": -340.0, "y": -96.0 },
                            "angle": 90.0
                            }
                    },
                "cars": [
                        {
                            "id": { "name": "Schumacher", "color": "red" },
                            "dimensions": { "length": 40.0, "width": 20.0, "guideFlagPosition": 10.0 }
                        },
                        {
                            "id": { "name": "Rosberg", "color": "blue" },
                            "dimensions": { "length": 40.0, "width": 20.0, "guideFlagPosition": 10.0 }
                        }
                    ],
                "raceSession": {
                        "laps": 2,
                        "maxLapTimeMs": 30000,
                        "quickRace": True
                        }
                }
        }}

gameStart = { "msgType": "gameStart", "data": None }

gameEnd = {
    "msgType": "gameEnd",
    "data": {
        "results": [],
        "bestLaps": [
                {
                    "car": { "name": "Schumacher", "color": "red" },
                    "result": { "lap": 2, "ticks": 3333, "millis": 20000 }
                },
                {
                    "car": { "name": "Rosberg", "color": "blue" },
                    "result": {}
                }
            ]
        }
    }

tournamentEnd = {"msgType": "tournamentEnd", "data": None}

def bot_tick(bot, inp):
    #print "<", inp
    out = bot.tick(inp)
    #print ">", out
    return out

import time

class CarModel:

    def __init__(self, car_id, dimensions=None):
        self.id = car_id
        self.dimensions = dimensions if dimensions else { "length": 40.0, "width": 20.0, "guideFlagPosition": 10.0 }
        piecePosition = { "pieceIndex": 0, "inPieceDistance": 0.0, "lane": { "startLaneIndex": 0, "endLaneIndex": 0 }, "lap": 0 }
        self.otherData = { "distance": 0.0, "velo": 0.0, "last": time.time(), "resptime": 0.0, "throttle": 0.0 }
        self.data = {
                "piecePosition": piecePosition,
                "angle": 0.0,
            }
        self.done = False

    def responded(self, t1, t2, resp):
        self.otherData["resptime"] = int(1000*(t2-t1))
        if resp["msgType"] == "throttle":
            self.set_throttle(float(resp["data"]))

    def set_throttle(self, throttle):
        self.otherData["throttle"] = throttle
        self.otherData["velo"] = 100.0*throttle

    def calc_displacement(self, t):
        deltaT = t - self.otherData["last"]
        displacement = self.otherData["velo"] * deltaT
        return displacement

    def update(self, t):
        self.otherData["distance"] += self.calc_displacement(t)
        self.otherData["last"] = t

def init_cars(data_race_cars):
    lane = 0
    cars = []
    for data in data_race_cars:
        car = CarModel(data["id"], data["dimensions"])
        car.data["piecePosition"]["lane"]["startLaneIndex"] = lane
        car.data["piecePosition"]["lane"]["endLaneIndex"] = lane
        cars.append(car)
        lane += 1
    return cars

if __name__ == '__main__':
    track = TrackGeometry(gameInit["data"]["race"]["track"])
    v = Display(track)
    cars = init_cars(gameInit["data"]["race"]["cars"])
    bots = dict([(car, Bot()) for car in cars])
    for (car, bot) in bots.items():
        bot_tick(bot, {"msgType": "yourCar", "data": car.id })
        bot_tick(bot, gameInit)
        bot_tick(bot, gameStart)
    gameTick = 0
    lapN = int(gameInit["data"]["race"]["raceSession"]["laps"])
    maxLapTimeMs = int(gameInit["data"]["race"]["raceSession"]["maxLapTimeMs"])
    millis = 0
    time0 = time.time()
    results = []
    while len([ car for car in cars if not car.done ]):
        t = time.time()
        millis = int(1000*(t-time0))
        gameTick += 1
        # server updates car models
        cps = []
        for car in cars:
            pp = car.data["piecePosition"]
            if not car.done:
                track.proceed(pp, car.calc_displacement(t))
                car.update(t)
                if pp["lap"] >= lapN: # done
                    car.done = True
                    results.append({ "car": car.id, "result": { "laps": pp["lap"], "ticks": gameTick, "millis": millis } })
            cps.append({ "id": car.id, "angle": car.data["angle"], "piecePosition": pp })
        # server sends message
        cpM = { "msgType": "carPositions", "data": cps, "gameId": "OIUHGERJWEOI", "gameTick": gameTick }
        for (car, bot) in bots.items():
            if not car.done:
                t1 = time.time()
                resp = bot_tick(bot, cpM)
                t2 = time.time()
                car.responded(t1, t2, resp)
        # draw
        v.clear()
        v.draw_track()
        for cp in cps:
            v.draw_car(cp)
        for (car0, bot) in bots.items():
            for car in bot.cars.values():
                if car0.id != car.id:
                    v.draw_car_belief(car0.id, { "id": car.id, "piecePosition": car.data["piecePosition"] })
        i = 0
        v.text(i, "T%05d #%04d %sHz" % (millis, gameTick, str(gameTick*1000/millis) if millis else "-"), "white")
        for car in cars:
            pp = car.data["piecePosition"]
            i += 1
            v.text(i, "D%05d L%02d P%02d/%03d %sHz v%d %s" % (int(car.otherData["distance"]), pp["lap"], pp["pieceIndex"], int(pp["inPieceDistance"]), str(1000/car.otherData["resptime"]) if car.otherData["resptime"] > 0 else "-", car.otherData["velo"], car.id["name"]), car.id["color"])
        v.show()
    gameEnd["data"]["results"] = results
    for (car, bot) in bots.items():
        bot.tick(gameEnd)
        bot.tick(tournamentEnd)
    print results
    v.loop()

