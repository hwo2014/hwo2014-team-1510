import json
import socket
import sys
import time

import numpy as np

import model
import random   # For generating more data to figure out the physics


"""
Learnings:
 - max speed is 10.0
 - we know the model of acceleration / deceleration, see model.py
 - at track piece ends we get a ~1.5 bump in speed (up or down, mostly down). see Speedometer for how we handle it (just ignoring it for now)
 - drifting doesn't seem to slow you down
 - max drift angle before crashing is 60.0


TODO:
  - Read our own car index, instead of a hard-coded 0, when looking for our position
  - Make speedometer / speed simulator take distanceFromCenter into account in bends
  - Figure out drift / slip angle physics. Until we have that, ideal speed is just a guess
  - Build a single, central representation of the track we can use to build our ideal lap based on the physics model
  - Introduce lane switching optimization, maybe as a second pass on the ideal lap builder
"""


telemetry_backend = lambda data: None


def telemetry(*args):
    data = reduce(lambda x,y: dict(x.items() + y.items()), args)
    data['tick'] = state['game_tick']
    telemetry_backend(data)


def signum(n):
    if n < 0:
        return -1
    return 1


def turn_angle(track, start_index):
    sig = signum(track['pieces'][start_index]['angle'])
    #radius = track['pieces'][start_index]['radius']
    angle = sig
    for piece in track['pieces'][start_index:] + track['pieces'][:start_index]:
        if 'angle' not in piece or signum(piece['angle']) != sig:
            return angle
        angle += piece['angle']
    return angle


def piece_length(piece, offset_radius=None):
    if 'length' in piece:
        return piece['length']
    else:
        if offset_radius is None:
            offset_radius = piece['radius']
        return (abs(piece['angle']) / 360) * 2 * np.pi * offset_radius


def distance_on_track(position, track):
    p = 0
    for i in xrange(position['pieceIndex']):
        p += piece_length(track['pieces'][i])
    return p + position['inPieceDistance']


def track_length(track):
    return sum(map(piece_length, track['pieces']))


profiles = {
    'keimola': {
    },
    'germany': {
    },
    'current': None
}


def read_profile(key, default=None):
    return profiles[profiles['current']].get(key, default)


class Vars(object):
    data = {}

    @classmethod
    def get(cls, name, min=0.0, max=1.0):
        if name not in cls.data:
            cls.data[name] = random.uniform(min, max)
        return cls.data[name]

    @classmethod
    def dump(cls):
        for key, value in cls.data.iteritems():
            print '%s\t%f' % (key, value)


state = {
    'throttle': 0,
    'game_tick': 0,
    'drift_angle': 0,
    'radius': 0,
    'last_radius': 0,
    'speed': 0,
    'angle': 0
}


class Speedomat(object):
    """
    Holds a target speed
    """
    def __init__(self, speedometer, target_speed):
        self.speed = 0
        self._target_speed = target_speed

    def set_throttle(self, val):
        telemetry({'target_speed': self._target_speed, 'throttle': val})
        if state['throttle'] == val:
            return None
        state['throttle'] = val
        return val

    def tick(self, data):
        ideal_throttle = model.ideal_throttle(state['speed'], self._target_speed)
        real_throttle = min(1, max(0, ideal_throttle))
        # print state['speed'], self._target_speed, ideal_throttle, real_throttle
        return self.set_throttle(real_throttle)

    @property
    def target_speed(self):
        return self._target_speed

    @target_speed.setter
    def target_speed(self, val):
        if val != self._target_speed:
            print 'Target speed: %f -> %f' % (self._target_speed, val)
        self._target_speed = val


class SpeedSimulator(object):
    # Get accurate data, a few ticks ahead of calculating it; faster response time ftw
    def __init__(self):
        self.speed = 0.0
        self.last_tick = 0

    def tick(self):
        while self.last_tick < state['game_tick']:
            self.speed += model.acceleration_at(self.speed, state['throttle'])
            self.last_tick += 1


class Speedometer(object):
    """
    Measures speed
    """
    measure_length = 3

    def __init__(self, track):
        self.max_speed = 0.0
        self.track = track
        self.speed = 0.0
        self.last_speed = 0.0
        self.history = []
        self.simulator = SpeedSimulator()

    def use_history(self, position):
        self.history.append(position)
        if len(self.history) <= self.measure_length:
            return position
        else:
            return self.history.pop(0)

    def tick(self, data):
        self.simulator.tick()

        current_position = data[0]['piecePosition']
        last_position = self.use_history(current_position)

        last_distance = distance_on_track(last_position, self.track)
        current_distance = distance_on_track(current_position, self.track)

        delta = current_distance - last_distance
        if delta < 0:
            delta = track_length(self.track) - last_distance + current_distance

        speed = float(delta) / self.measure_length
        if abs(speed - self.last_speed) >= 0.7:
            pass
        else:
            self.last_speed = self.speed
            self.speed = speed

        # if abs(self.simulator.speed - self.speed) > 0.5:
            # print 'Correcting modelled speed: %f -> %f' % (self.simulator.speed, self.speed)

        state['speed'] = self.simulator.speed
        return self.simulator.speed
        # return self.speed


class Accelerometer(object):
    """
    Measures acceleration
    """
    def __init__(self, speedometer):
        self.speedometer = speedometer
        self.last = 0

    def tick(self, data):
        delta = self.speedometer.speed - self.last
        # if delta < 0:
        #     print self.last, '\t', self.speedometer.speed, '\t', delta
        self.last = self.speedometer.speed
        try:
            if 'gameTick' in data[0] and telemetry:
                telemetry(data[0], {'acceleration': delta, 'speed': self.last})
        except:
            pass


class SplitTimer(object):
    """
    Timing!
    """
    def __init__(self):
        self.last_piece_index = 0
        self.split_timer = 0
        self.lap_timer = 0

    def tick(self, data):
        if 'gameTick' not in data[0]:
            return
        current_piece_index = data[0]['piecePosition']['pieceIndex']
        game_tick = state['game_tick']
        if current_piece_index != self.last_piece_index:
            print 'Split %d: %d   Lap: %d' % (self.last_piece_index, game_tick - self.split_timer, game_tick - self.lap_timer)
            self.split_timer = game_tick
        if current_piece_index < self.last_piece_index:
            print 'LAP'
            with open('db', 'a') as f:
                Vars.data['laptime'] = game_tick - self.lap_timer
                f.write(str(Vars.data))
            self.lap_timer = game_tick
        self.last_piece_index = current_piece_index


class EntrySpeedByRadius(object):
    """
    Main logic, part 1: speed based on the track and current lane
    """
    def __init__(self, track, speedometer):
        self.flatout_radius = 155.0

        self.track = track
        self.entries = {}  # entry to [lane][pieceId]
        self.speedometer = speedometer

        for lane in track['lanes']:
            distance_from_center = lane['distanceFromCenter']
            self.entries[lane['index']] = {}
            pos = 0
            corner_angle = None
            corner_length = 0
            for i, piece in enumerate(track['pieces']):
                if piece.get('angle', None) != corner_angle and corner_length > 1:
                    # Slow in, fast out; accelerate out from the apex (assuming that's at half the corner)
                    apex = i - corner_length/2
                    for j in range(apex, i):
                        if j not in self.entries[lane['index']]:
                            continue
                        self.entries[lane['index']][j]['speed'] = model.max_speed
                    corner_length = 0
                elif 'angle' in piece and piece['angle'] == corner_angle:
                    corner_length += 1
                corner_angle = piece.get('angle', None)

                if 'radius' not in piece:
                    pos += piece_length(piece)
                    continue

                radius = piece['radius'] - signum(piece['angle']) * distance_from_center
                # print lane, radius
                # if radius == 90:
                #     speed = 6.0
                # elif radius == 110:
                #     speed = 7.0
                # elif radius == 190:
                #     speed = 9.0
                # elif radius == 210:
                #     speed = 10.0
                # else:
                speed = min(model.max_speed, model.max_speed * (float(radius) / self.flatout_radius))
                self.entries[lane['index']][i] = {
                    'index': i,
                    'speed': speed,
                    'pos': pos,
                    'angle': piece['angle']
                }
                pos += piece_length(piece, radius)
        # print self.entries

    def tick(self, data):
        position = data[0]['piecePosition']
        current_lane = position['lane']['endLaneIndex']
        target_speed, target_lane_switch = self.optimize(position, self.entries[current_lane]), None
        return target_speed, target_lane_switch
        # if (current_lane - 1) in self.entries:
        #     candidate_target_speed = self.optimize(position, self.entries[current_lane-1])
        #     if candidate_target_speed > target_speed:
        #         target_speed, target_lane_switch = candidate_target_speed, 'Left'
        # if (current_lane + 1) in self.entries:
        #     candidate_target_speed = self.optimize(position, self.entries[current_lane+1])
        #     if candidate_target_speed > target_speed:
        #         target_speed, target_lane_switch = candidate_target_speed, 'Right'
        # return target_speed, target_lane_switch

    def optimize(self, position, entries):
        target_speed = entries.get(position['pieceIndex'], {'speed': model.max_speed})['speed']
        my_distance_on_track = distance_on_track(position, self.track)
        for entry in entries.itervalues():
            to_entry = entry['pos'] - my_distance_on_track
            if to_entry < 0:
                to_entry += track_length(self.track)
            entry_speed = entry['speed']
            # entry_speed += entry_speed * (signum(entry['angle']) * state['drift_angle'] / model.crash_drift_angle)
            distance_to_target_speed = model.distance_to_slow_down(self.speedometer.speed, entry_speed)
            if distance_to_target_speed >= to_entry:
                target_speed = min(target_speed, entry['speed'])
        return target_speed


class RacingLine(object):
    """
    Main logic, part 2: switch lanes to optimize racing line
    """
    max_radius = 150
    min_angle = 90

    def __init__(self, track):
        self.track = track

    def tick(self, data):
        current_position = data[0]['piecePosition']
        piece = self.track['pieces'][current_position['pieceIndex']]

        if not 'radius' in piece:
            radius = 0
        else:
            radius = piece['radius'] * signum(piece['angle'])
            state['angle'] = piece['angle']
        if radius != state['radius']:
            state['last_radius'] = state['radius']
            state['radius'] = radius

        return

        if 'angle' not in piece or not piece.get('switch', False):
            return

        # 1: if we're drifting, let's widen the turn
        if abs(data[0]['angle']) > 30.0:
            if piece['angle'] > 0:
                return 'Right'
            return 'Left'

        # 2: if we're not drifting, let's tighten it
        if abs(data[0]['angle']) < 20.0:
            if piece['angle'] < 0:
                return 'Left'
            return 'Right'

        # 3: prepare for the next turn by moving into the inner lane
        for i in range(current_position['pieceIndex']+1, len(self.track['pieces'])) + range(0, current_position['pieceIndex']):
            piece = self.track['pieces'][i]
            if 'radius' not in piece:
                continue
            angle = turn_angle(self.track, i)
            if piece['radius'] <= self.max_radius and abs(angle) >= self.min_angle:
                if piece['angle'] > 0:
                    return 'Right'
                return 'Left'


# An experiment at controlling throttle based on drift angle
class Drifter(object):
    measure_length = 3
    boost = 0.5

    def __init__(self):
        self.history = []

    def use_history(self, angle):
        self.history.append(angle)
        if len(self.history) <= self.measure_length:
            return angle
        else:
            return self.history.pop(0)

    def tick(self, data):
        current_angle = abs(state['drift_angle'])
        last_angle = self.use_history(current_angle)
        delta = current_angle - last_angle
        if current_angle + (delta * 10) > 60:
            return -1
        return 0


class AwesomeBot(object):
    def __init__(self, socket, name, key):
        self.track_name = 'keimola'
        profiles['current'] = self.track_name
        self.speedometer = None
        self.speedomat = None
        self.entry_speed_by_radius = None
        self.accelerometer = None
        self.timer = SplitTimer()
        self.drifter = Drifter()
        self.racing_line = None
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("joinRace", {"botId": {"name": self.name,
                                               "key": self.key},
                                     "trackName": self.track_name,
                                     "carCount": 1})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_game_init(self, data):
        print("Initializing")
        print data
        self.track = data['race']['track']
        state['track'] = self.track
        self.car = data['race']['cars'][0]
        self.speedometer = Speedometer(data['race']['track'])
        self.speedomat = Speedomat(self.speedometer, 4)
        self.entry_speed_by_radius = EntrySpeedByRadius(data['race']['track'], self.speedometer)
        self.accelerometer = Accelerometer(self.speedometer)
        self.racing_line = RacingLine(data['race']['track'])

    def on_car_positions(self, data):
        start = time.clock()
        telemetry(state)
        # try:
        #     r = self.track['pieces'][data[0]['piecePosition']['pieceIndex']]['radius']
        #     h = self.car['dimensions']['length'] / 2 - self.car['dimensions']['guideFlagPosition']
        #     beta = np.arctan(r / h)
        #     f = (0.5 * state['speed']**2 * np.sin(beta))
        #     d = (data[0]['angle'] - state['drift_angle'])
        # except:
        #     pass
        state['drift_angle'] = data[0]['angle']
        state['distance_travelled'] = distance_on_track(data[0]['piecePosition'], state['track'])

        # try:
        #     print state, state['speed'] / state['radius']
        # except:
        #     pass

        self.timer.tick(data)
        self.speedometer.tick(data)
        self.accelerometer.tick(data)

        target_entry_speed, target_lane = self.entry_speed_by_radius.tick(data)
        drifter_speed_mod = self.drifter.tick(data)

        self.speedomat.target_speed = target_entry_speed# + drifter_speed_mod

        target_lane_dir = self.racing_line.tick(data)
        # if target_lane_dir is not None:
        #     return self.msg('switchLane', target_lane_dir)

        target_throttle = self.speedomat.tick(data)

        telemetry({'processing_time_seconds': time.clock() - start})

        if target_throttle is not None:
            return self.throttle(target_throttle)

        if target_lane is not None:
            return self.msg('switchLane', target_lane)

        # if random.randint(0,2) % 2:
        #     return self.throttle(state['throttle'])
        # return self.msg('switchLane', 'Right')
        return self.throttle(state['throttle'])

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        print data
        self.ping()

    def msg_loop(self):
        Vars.dump()
        msg_map = {
            'gameInit': self.on_game_init,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'yourCar': self.on_your_car
        }
        socket_file = self.socket.makefile()
        while not socket_file.closed:
            line = socket_file.readline()
            if not line:
                continue
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            try:
                data[0]['gameTick'] = msg['gameTick']
                state['game_tick'] = msg['gameTick']
            except:
                pass
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()


def main(host='hakkinen.helloworldopen.com', port=8091, name='K.I.T.T.E.N.', key='Wq6pfczH4Tvrbg'):
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*[host, port, name, key]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = AwesomeBot(s, name, key)
    bot.run()


if __name__ == "__main__":
    def tb(data):
        print data
    telemetry_backend = tb
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        main(*sys.argv[1:])
