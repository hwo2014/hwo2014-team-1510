import sys
import socket

from main import AwesomeBot
from poc import TrackGeometry, Display


class VisualisedAwesomeBot(AwesomeBot):
    def __init__(self, socket, name, key):
        super(VisualisedAwesomeBot, self).__init__(socket, name, key)
        self.vis_track = None

    def on_game_init(self, data):
        super(VisualisedAwesomeBot, self).on_game_init(data)
        self.vis_track = TrackGeometry(data['race']['track'])
        self.display = Display(self.vis_track)
        self.laps = data['race']['raceSession']['laps']
        self.max_lap_time_ms = data['race']['raceSession']['maxLapTimeMs']

    def on_car_positions(self, data):
        super(VisualisedAwesomeBot, self).on_car_positions(data)
        self.display.clear()
        self.display.draw_track()
        for item in data:
            self.display.draw_car(item)
        self.display.show()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = VisualisedAwesomeBot(s, name, key)
        bot.run()

