"""
Run a race collecting data for our model of the physics, and showing telemetry data
Usage:

pip install matplotlib
ipython --matplotlib tk
import telemetry; telemetry.run()

Stopping:
pkill -f python

Yes, really. Go ahead, fix it :)
"""

import socket
from main import AwesomeBot

import pylab as pl
import threading as thrd
import numpy as np
import signal
import collections
import pickle
import functools
import csv

# SIGINT handling is required for the main thread to catch
# keyboard interrupts
def sigint(signum, frame):
    import sys
    sys.exit()
signal.signal(signal.SIGINT, sigint)

should_update_pickle = False


def load_pickle_data():
    try:
        with open(graph_data_pickle_filename, 'r') as f:
            return pickle.load(f)
    except:
        return {}


race_data = collections.defaultdict(dict)

pl.ion()

max_time = 2000
by_speed = np.linspace(0, 10, 10000)
by_time = np.linspace(0, max_time, max_time)
by_drift_angle = np.linspace(0, 2, 100000)


graph_data_pickle_filename = '../graphs/data.pickle'


# Model data, saved
graph_data = {
    'acceleration_by_speed': np.full(by_speed.shape, None),
    'deceleration_by_speed': np.full(by_speed.shape, None)
}
graph_data.update(load_pickle_data())


figures = {
    'main': pl.figure(1, figsize=(12,10)),
    'telemetry': pl.figure(2, figsize=(12,10))
}
figures['main'].canvas.set_window_title('Data collection!')
figures['telemetry'].canvas.set_window_title('K.I.T.T.E.N. Telemetry station')


plots = {
    'drift_angle_and_delta': figures['main'].add_subplot(211, xlim=(-1, 3), ylim=(-10, 70), xlabel='pow(v,2)/r', ylabel='beta'),
    'acceleration_by_speed': figures['main'].add_subplot(212, xlim=(-1, 11), ylim=(-1, 1), xlabel='speed', ylabel='acceleration'),
    'speed': figures['telemetry'].add_subplot(511, xlim=(0,max_time), ylim=(-1, 11), xlabel='tick', ylabel='speed'),
    'throttle': figures['telemetry'].add_subplot(512, xlim=(0,max_time), ylim=(-0.1, 1.1), xlabel='tick', ylabel='throttle'),
    'drift_angle': figures['telemetry'].add_subplot(513, xlim=(0,max_time), ylim=(-70,70), xlabel='tick', ylabel='drift angle'),
    'radius': figures['telemetry'].add_subplot(514, xlim=(0,max_time), ylim=(-200,200), xlabel='tick', ylabel='turn radius'),
    'proctime': figures['telemetry'].add_subplot(515, xlim=(0,max_time), ylim=(0,0.02), xlabel='tick', ylabel='processing time'),
}


graphs = {
    'acceleration_by_speed': plots['acceleration_by_speed'].plot(by_speed, graph_data['acceleration_by_speed'], '.')[0],
    'deceleration_by_speed': plots['acceleration_by_speed'].plot(by_speed, graph_data['deceleration_by_speed'], '.')[0],
}

graphs_by_time_spec = {
    'speed': ['speed', 'target_speed'],
    'throttle': ['throttle'],
    'drift_angle': ['drift_angle'],
    'radius': ['radius'],
    'proctime': ['processing_time_seconds']
}
graphs_by_time_names = []
for plot_name, data_names in graphs_by_time_spec.iteritems():
    ax = plots[plot_name]
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.85, box.height])
    for data_name in data_names:
        graph_data[data_name] = np.full(by_time.shape, None)
        graphs[data_name] = ax.plot(by_time, graph_data[data_name], '-', label=data_name)[0]
        graphs_by_time_names.append(data_name)
    plots[plot_name].legend(loc='center left', bbox_to_anchor=(1, 0.5), fontsize='small')

plots['throttle'].axhline(y=1, c='red', zorder=0)
plots['throttle'].axhline(y=0, c='red', zorder=0)
plots['drift_angle'].axhline(y=60, c='red', zorder=0)
plots['drift_angle'].axhline(y=-60, c='red', zorder=0)
plots['proctime'].axhline(y=1.0/60, c='red', zorder=0)

def add_drift_angle_and_delta_plot(radius):
    key = 'drift_angle_and_delta_%d' % radius
    if key not in graph_data:
        graph_data[key] = np.full(by_drift_angle.shape, None)
    graphs[key] = plots['drift_angle_and_delta'].plot(by_drift_angle, graph_data['drift_angle_and_delta_%d' % radius], '.', label='radius=%d' % radius)[0]
drift_angle_radiuses = [0, 50, 100, 150, 200]
map(add_drift_angle_and_delta_plot, drift_angle_radiuses)

plots['drift_angle_and_delta'].legend(loc='lower right', fontsize='medium')

pl.show()


def set_acceleration(data):
    if not ('speed' in data and 'acceleration' in data):
        return
    speed = data['speed']
    acceleration = data['acceleration']
    if acceleration > 1:
        # TODO figure out what's up with these
        return
    if acceleration > 0:
        graph_data['acceleration_by_speed'][speed*1000] = acceleration
    else:
        graph_data['deceleration_by_speed'][speed*1000] = acceleration


def set_drift_angle_by_speed(data):
    try:
        speed = data.get('speed', 0)
        drift_angle = abs(data['drift_angle'])
        radius = abs(data['radius'])
        key2 = np.power(speed, 2) / radius
        # for representation
        key2 *= 50000
        if np.isinf(key2) or np.isnan(key2):
            return
    except:
        return
    if radius in drift_angle_radiuses:
        key = 'drift_angle_and_delta_%d' % radius
        old = graph_data[key][key2]
        if np.isnan(old) or old < drift_angle:
            graph_data[key][key2] = drift_angle
    else:
        print 'not plotting radius %s' % radius


def set_by_time(data):
    for key in graphs_by_time_names:
        if key in data:
            graph_data[key][data['tick'] % max_time] = data[key]


def update_graphs():
    for key, ydata in graph_data.iteritems():
        if key not in graphs:
            continue
        graph = graphs[key]
        graph.set_ydata(ydata)
    pl.draw()


def update_graph_data_pickle():
    pickle_data = load_pickle_data()
    for key, data in graph_data.iteritems():
        if key not in pickle_data:
            pickle_data[key] = data
        else:
            for i, val in enumerate(data):
                if val is not None:
                    pickle_data[key][i] = val
    print 'saving', pickle_data.keys()
    # for key in pickle_data.iterkeys():
    #     pickle_data[key] = pickle_data[key].dumps()
    with open(graph_data_pickle_filename, 'w') as f:
        pickle.dump(pickle_data, f)


def set(data):
    global should_update_pickle
    race_data[data['tick']].update(data)
    set_acceleration(data)
    set_by_time(data)
    set_drift_angle_by_speed(data)
    if data['tick'] % 100 == 0:
        should_update_pickle = True


class Run(thrd.Thread):
    host='hakkinen.helloworldopen.com'
    port=8091
    name='K.I.T.T.E.N.'
    key='Wq6pfczH4Tvrbg'

    def __call__(self, *args, **kwargs):
        import main
        main.telemetry_backend = set
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*[self.host, self.port, self.name, self.key]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self.host, int(self.port)))
        bot = AwesomeBot(s, self.name, self.key)
        bot.run()


class DriftDataDumper(object):
    filename = '../graphs/drift.csv'
    fieldnames = ['game_tick', 'distance_travelled', 'radius', 'speed', 'drift_angle']

    def __init__(self):
        self.next_tick = 0

    def dump(self):
        try:
            with open(self.filename, 'a') as f:
                csv_writer = csv.DictWriter(f, self.fieldnames, extrasaction='ignore')
                while race_data.has_key(self.next_tick):
                    csv_writer.writerow(race_data[self.next_tick])
                    self.next_tick += 1
        except Exception as e:
            print 'failed to write drift data: %s' % e


# Init and start threads
def run():
    global should_update_pickle
    t0 = thrd.Thread(target=Run())
    t0.start()
    drift_data_dumper = DriftDataDumper()
    while True:
        update_graphs()
        drift_data_dumper.dump()
        if should_update_pickle:
            update_graph_data_pickle()
            should_update_pickle = False
        pl.pause(0.05)

if __name__ == '__main__':
    run()
