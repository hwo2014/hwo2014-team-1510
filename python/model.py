max_speed = 10.0
crash_drift_angle = 60.0


def acceleration_at(speed, throttle):
    return 0.02 * (max_speed - speed) - 0.2 * (1 - throttle)


def ideal_throttle(current_speed, target_speed):
    # d = (0.02 * (max_speed - speed)) - 0.2 * (1 - throttle)
    # d - (0.02 * (max_speed - speed)) = - 0.2 * (1 - throttle)
    # (0.02 * (max_speed - speed)) - d = 0.2 * (1 - throttle)
    # 5 * ((0.02 * (max_speed - speed)) - d) =  1 - throttle
    # 1 - 5 * ((0.02 * (max_speed - speed)) - d) =  throttle
    delta = (target_speed - current_speed) / 5
    return 1 - 5 * ((0.02 * (max_speed - current_speed)) - delta)


def distance_to_slow_down(current_speed, target_speed):
    # TODO: find closed form
    distance = 0
    ticks = 0
    while current_speed > target_speed:
        ticks += 1
        distance += current_speed
        current_speed += acceleration_at(current_speed, 0)
    return distance
